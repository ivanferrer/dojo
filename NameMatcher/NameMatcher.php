<?php

class NameMatcher
{
  private $name;
  const BASE = "Luiz Fernando Rodrigues Vid";

  public function __construct($name = null)
  {
    if (is_null($name)){
       throw new InvalidArgumentException('Parameter is missing');
    }

    if (str_word_count($name) < 2) {
     throw new InvalidArgumentException('Two or more words Parameter Needed');
    }

    $this->name = $name;
  }

  public function match()
  {
    $namesAsArray = explode(' ', strtoupper($this->name));
    $baseAsArray = explode(' ', strtoupper(self::BASE));

    if(array_shift($namesAsArray) != array_shift($baseAsArray)){
      return false;
    }

    foreach ($namesAsArray as $name) {
      if (! array_search($name, $baseAsArray)) {
        return false;
      }
    }



    return true;
  }
}

