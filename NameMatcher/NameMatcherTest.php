<?php

require "NameMatcher.php";

class NameMatcherTest extends PHPUnit_Framework_TestCase
{

  /**
  * @expectedException        InvalidArgumentException
  * @expectedExceptionMessage Parameter is missing
  */
  public function testShouldNotInstantiateWithoutStringParameter()
  {
    $instance = new NameMatcher();
  }

/**
  * @expectedException        InvalidArgumentException
  * @expectedExceptionMessage Two or more words Parameter Needed
  */
  public function testShouldAcceptAtLeastTwoNames()
  {
    $instance = new NameMatcher('Luiz');

  }

  public function testShouldMatch()
  {
    $instance = new NameMatcher('Luiz Fernando Rodrigues Vid');

    $expected = true;
    $actual = $instance->match();

    $this->assertEquals($expected, $actual);
  }

  public function testShouldNotMatch()
  {
    $instance = new NameMatcher('Fernando Fernando Rodrigues');

    $expected = false;
    $actual = $instance->match();

    $this->assertEquals($expected, $actual);
  }

  public function testShouldMatchWithTwoNames()
  {
    $instance = new NameMatcher('Luiz Fernando');

    $expected = true;
    $actual = $instance->match();

    $this->assertEquals($expected, $actual);
  }

  public function testShouldMatchWithFirstNameAndLastName()
  {
    $instance = new NameMatcher('Luiz Vid');

    $expected = true;
    $actual = $instance->match();

    $this->assertEquals($expected, $actual);
  }

  public function testShouldNotMatchWithOnlyLastNames()
  {
    $instance = new NameMatcher('Rodrigues Vid');

    $expected = false;
    $actual = $instance->match();

    $this->assertEquals($expected, $actual);
  }

  public function testShouldMatchWithLastTwoNames()
  {
    $instance = new NameMatcher('Luiz Rodrigues Vid');

    $expected = true;
    $actual = $instance->match();

    $this->assertEquals($expected, $actual);
  }

  public function testShouldMatchCaseInsensitive()
  {
    $instance = new NameMatcher('lUiZ fErNaNdO rOdrigues ViD');

    $expected = true;
    $actual = $instance->match();

    $this->assertEquals($expected, $actual);
  }

   public function testShouldNotMatchWhenOutOfOrder()
  {
    $instance = new NameMatcher('lUiZ rodrigues fErNaNdO ViD');

    $expected = false;
    $actual = $instance->match();

    $this->assertEquals($expected, $actual);
  }


}
