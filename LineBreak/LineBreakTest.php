<?php

require "LineBreak.php";

class LineBreakTest extends PHPUnit_Framework_TestCase
{
	public function testShouldReturnTextInOneLine()
	{
		$expected = "Um pequeno jabuti";
		$lineBreak = new LineBreak();

		$text = 'Um pequeno jabuti';
		$actual = $lineBreak->run($text, 20);

		$this->assertEquals($expected, $actual);
	}

	public function testShouldReturntextInTwoLines()
	{
		$expected = "Um pequeno jabuti\nxereta viu dez";
		$lineBreak = new LineBreak();

		$text = 'Um pequeno jabuti xereta viu dez';
		$actual = $lineBreak->run($text, 20);

		$this->assertEquals($expected, $actual);
	}

	public function testShouldReturntextInThreeLines()
	{
		$expected = "Um pequeno jabuti\nxereta viu dez\ncegonhas felizes.";
		$lineBreak = new LineBreak();

		$text = 'Um pequeno jabuti xereta viu dez cegonhas felizes.';
		$actual = $lineBreak->run($text, 20);

		$this->assertEquals($expected, $actual);
	}
}