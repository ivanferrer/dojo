<?php

require "Intervals.php";

class IntervalsTest extends PHPUnit_Framework_TestCase
{
  public function testShouldReturnArrayWithOneNumber()
  {
    $intervals = new Intervals();

    $expected = ['1'];
    $actual = $intervals->run([1]);

    $this->assertEquals($expected, $actual);
  }

  public function testShouldReturnArrayWithTwoNumbers()
  {
    $intervals = new Intervals();

    $expected = ['1-2'];
    $actual = $intervals->run([1,2]);

    $this->assertEquals($expected, $actual);
  }

  public function testShouldReturnArrayWithArrayNumbersSequence()
  {
    $intervals = new Intervals();

    $arrayCollection = [
      '1-3',
      '5',
      '8',
    ];

    $expected = $arrayCollection;
    $actual = $intervals->run([1,2,3,5,8]);
    $this->assertEquals($expected, $actual);
  }
}
