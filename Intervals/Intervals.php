<?php

class Intervals
{

  public function run(array $arrayOfNumbers)
  {
  	$result = [];
  	$intervalIndex = 0;

    $result[$intervalIndex][] = array_shift($arrayOfNumbers);
    $lastNumber = $result[$intervalIndex][0];

  	foreach ($arrayOfNumbers as $key => $number) {
	   	if ($lastNumber + 1 != $number) {
        $intervalIndex++;
      }
      $result[$intervalIndex][] = $number;

	   	$lastNumber = $number;
  	}

     return array_map(function($array){
        if (count($array) == 1) {
          return $array[0];
        }
        return array_shift($array).'-'.end($array);
     }, $result);
  }
}

/*
1
$v[0] = 1;
2
$v[] = 2;

4
$v[]


*/
