<?php

require "Jokenpo.php";

class JokenpoTest extends PHPUnit_Framework_TestCase
{
  public function testShouldInstantiateWithTwoParameters()
  {
    $actual = new Jokenpo('pedra','tesoura');
    $this->assertInstanceOf('Jokenpo', $actual);
  }

  /**
   * @expectedException InvalidArgumentException
  */
  public function testShouldNotInstantiateWithInvalidParameters()
  {
    new Jokenpo('bolinha','tesoura');
  }

  public function testShouldReturnPaperWhenPaperAndStone()
  {
    $expected = 'papel';
    $jokenpo = new Jokenpo('papel', 'pedra');
    $actual = $jokenpo->getResult();

    $this->assertEquals($expected, $actual);
  }

  public function testShouldReturnStoneWhenScissorAndStone()
  {
    $expected = 'pedra';
    $jokenpo = new Jokenpo('tesoura', 'pedra');
    $actual = $jokenpo->getResult();

    $this->assertEquals($expected, $actual);
  }
}
