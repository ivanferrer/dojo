<?php

class FizzBuzz
{
  private $listSize;

  public function __construct($listSize)
  {
    $this->listSize = $listSize;
  }

  public function run()
  {
    $resultList = [];
    for ($i = 1; $i <= $this->listSize; $i++) {
      $resultList[$i] = $this->getFizzBuzz($i);
    }
    return $resultList;
  }

  public function getFizzBuzz($number)
  {
    $result = '';
    if ($number % 7 === 0) {
      $result .= 'Fizz';
    }
    return ($result) ? $result : $number;
  }

}
