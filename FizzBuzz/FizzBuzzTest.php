<?php

include('FizzBuzz.php');

class FizzBuzzTest extends PHPUnit_Framework_TestCase
{

  public function testShouldReturnOne()
  {
    $fizzBuzz = new FizzBuzz(1);

    $expects = [1];
    $result = $fizzBuzz->run();

    $this->assertEquals($expects, $result);
  }

  public function testShouldReturnOneAndTwo()
  {
    $fizzBuzz = new FizzBuzz(2);

    $expects = [1,2];
    $result = $fizzBuzz->run();

    $this->assertEquals($expects, $result);
  }

  public function testShouldReturnOneAndTwoAndThreeAsFizz()
  {
    $fizzBuzz = new FizzBuzz(3);

    $expects = [1,2,'Fizz'];
    $result = $fizzBuzz->run();

    $this->assertEquals($expects, $result);
  }

  public function testShouldReturnFiveAsBuzz()
  {
    $fizzBuzz = new FizzBuzz(5);

    $expects = [1,2,'Fizz',4,'Buzz'];
    $result = $fizzBuzz->run();

    $this->assertEquals($expects, $result);
  }

  public function testShouldReturnFifteenAsFizzBuzz()
  {
    $fizzBuzz = new FizzBuzz(15);

    $expects = [
      1,
      2,
      'Fizz',
      4,
      'Buzz',
      'Fizz',
      7,
      8,
      'Fizz',
      'Buzz',
      11,
      'Fizz',
      13,
      14,
      'FizzBuzz'
    ];
    $result = $fizzBuzz->run();

    $this->assertEquals($expects, $result);
  }
}
