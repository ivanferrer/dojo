<?php

require "ScalarProduct.php";

class ScalarProductTest extends PHPUnit_Framework_TestCase
{
    public function testShouldReturnOne()
    {
      $expected = 1;

      $scalarProduct = new ScalarProduct();

      $actual = $scalarProduct->calc([1],[1]);

      $this->assertEquals($expected, $actual);
    }

    public function testShouldReturnTwo()
    {
      $expected = 2;

      $scalarProduct = new ScalarProduct();

      $actual = $scalarProduct->calc([1,1],[1,1]);

      $this->assertEquals($expected, $actual);
    }

    public function testShouldReturnThree()
    {
      $expected = 2;

      $scalarProduct = new ScalarProduct();

      $actual = $scalarProduct->calc([1,2,2],[1,1]);

      $this->assertEquals($expected, $actual);
    }


}
