<?php

class ScalarProduct
{
  public function calc(array $vectorA, array $vectorB)
  {
    $scalarProduct = 0;
    for($vectorIndex = 0; $vectorIndex < count($vectorA); $vectorIndex++) {
      $scalarProduct += $vectorA[$vectorIndex] * $vectorB[$vectorIndex];
    }

    return $scalarProduct;
  }

  public function getSmallerVectorLength(array $vectorA, array $vectorB)
  {
    if ($vectorA < $vectorB) {
      return count($vectorA);
    }

    return count($vectorB);
  }
}
