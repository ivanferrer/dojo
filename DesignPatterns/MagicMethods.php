<?php
/**
 * Created by PhpStorm.
 * User: dojo
 * Date: 1/29/16
 * Time: 11:26 AM
 */

class MagicMethods {

    private $data = null;
    private $name = null;
    private $age = null;
    private $address = null;


    public function __call($method, $params)
    {
      $calledMethodType = substr($method, 0, 3);
        $varName = substr($method, 3, strlen($method) -1);
        $varName = lcfirst($varName);
        switch($calledMethodType){
            case 'get' :

                return $this->$varName;
                break;
            case 'set' :
                $this->$varName = $params[0];
                return $this;
                break;
            default:
                throw new Exception("Method {$method} does not exists");
                break;
        }

    }

    public function __toString()
    {
        return "\n".implode(', ',get_object_vars($this))."\n";
    }

}


$myMagic = new MagicMethods();

$myMagic->setData((new DateTime('1991-09-05'))->format('d/m/Y'))
    ->setName('Nome: John')
    ->setAge('idade: 24')
    ->setAddress('Endereço: Rua whatever');

echo $myMagic;