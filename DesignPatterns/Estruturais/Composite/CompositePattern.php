<?php

/**
 * S.O.L.I.D
 *
 * Polimorfismo, Princípio de segregação de interfaces de Barbara Liskov e
 * CompositePattern < - Composição,
 * Princípio aberto e fechado
 */



$string = "a0s9d80sad08asd0asdja0dka9kas009das679868985459k3jk3j3";

interface Filter
{

    public function filter($string);
}

interface FilterChain extends Filter
{

    public function addFilter(Filter $filter);
}

class FilterRemoveA implements Filter
{

    public function filter($string)
    {
        return str_replace('a', '', $string);
    }
}

class FilterRemoveZero implements Filter
{

    public function filter($string)
    {
        return str_replace('0', '', $string);
    }
}

class FilterRemoveNumberSix implements Filter
{

    public function filter($string)
    {
        return str_replace('6', '', $string);
    }
}

class FilterRemoveNumberEight implements Filter
{

    public function filter($string)
    {
        return str_replace('8', '', $string);
    }
}

class FilterChainCollection implements FilterChain
{
    private $filterCollection = [];

    public function addFilter(Filter $filter)
    {
        $this->filterCollection[] = $filter;
    }

    public function filter($string)
    {
        foreach ($this->filterCollection as $filter) {
            $string = $filter->filter($string);
        }

        return $string;
    }
}

$filter = new FilterChainCollection();
$filter->addFilter(new FilterRemoveA());
$filter->addFilter(new FilterRemoveZero());
$filter->addFilter(new FilterRemoveNumberSix());

$filter2 = new FilterChainCollection();
$filter2->addFilter($filter);
$filter2->addFilter(new FilterRemoveNumberEight());


echo $filter2->filter($string);