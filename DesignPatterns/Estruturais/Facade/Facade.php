<?php

namespace Estruturais\Facade;

interface ICelular
{
   public function __construct();

}

class Celular implements ICelular
{

    private $teclado = 'a-z';
    private $os ='Android';
    private $tela = 'Touch';
    private $memoria = '2.5GB';
    private $bateria = '2 HMA';
    private $chip = 'Vivo';
    private $acoesMsg;
    private $acoesDisc;

    public function __construct()
    {
        $this->acoesMsg = new AcoesMensagem();
        $this->acoesDisc = new AcoesDiscagem();
    }

    public function talk()
    {
        $ac  = $this->acoesDisc->discar();
        $ac .= $this->acoesDisc->chamar();
        return $ac;
    }

}

class AcoesMensagem
{

    public function enviarSMS()
    {

    }

    public function gravarAgenda()
    {

    }
}

class AcoesDiscagem
{
    public function discar()
    {

    }

    public function chamar()
    {

    }
}

echo (new Celular())
          ->talk();
