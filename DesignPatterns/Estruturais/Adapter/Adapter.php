<?php
/**
 * Created by PhpStorm.
 * User: dojo
 * Date: 1/20/16
 * Time: 11:35 AM
 */
// Exemplo 1
namespace Estruturais\Adapter;


class ControleTVPhillips
{
    public function ligarPhillips()
    {

    }
}

class ControlerTVSamsung
{
    public function ligarSamsung()
    {

    }

    public function antesDeLigarFacaIsso($param)
    {

    }

}

class ControleAdapter implements ControleRemotoAdapter
{
    private $controle1;
    private $controle2;

    public function __construct(ControleTVPhillips $controle1, ControlerTVSamsung $controle2)
    {
        $this->controle1 = $controle1;
        $this->controle2 = $controle2;
    }

    public function ligar()
    {
        $param =  $this->controle1->ligarPhillips();
        echo $this->controle2->antesDeLigarFacaIsso($param);
        echo $this->controle2->ligarSamsung();
    }
}

interface ControleRemotoAdapter
{
    public function ligar();
}

// Exemplo 2


interface Book
{
    public function turnPage();
    public function closeBook();
}

interface PaperBook
{
    public function virarPagina();
    public function fecharLivro();
}

interface EBook
{
    public function nextPage();
    public function closeBook();
}

class HarryPotterPapel implements PaperBook
{
    public function virarPagina()
    {

    }

    public function fecharLivro()
    {

    }
}

class HarryPotterEbook implements EBook
{
    public function nextPage()
    {

    }

    public function closeBook()
    {

    }
}

class BookAdapter implements Book
{

    private $book = null;

    public function __contruct(PaperBook $book)
    {
        $this->book = $book;
    }

    public function turnPage()
    {
        $this->book->virarPagina();
    }

    public function closeBook()
    {
        $this->book->fecharLivro();
    }
}

class EbookAdapter implements Book
{

    private $book = null;

    public function __contruct(EBook $book)
    {
        $this->book = $book;
    }

    public function turnPage()
    {
        $this->book->nextPage();
    }

    public function closeBook()
    {
        $this->book->closeBook();
    }
}


$harryEbook = new HarryPotterEbook();
$harryPapel = new HarryPotterPapel();


$livros = [];
$adapterEbook = new EbookAdapter($harryEbook);
$adapterPapel = new BookAdapter($harryPapel);

$livros[] = $adapterEbook;
$livros[] = $adapterPapel;



foreach($livros as $livro){
    $livro->turnPage();
    $livro->closeBook();
}




