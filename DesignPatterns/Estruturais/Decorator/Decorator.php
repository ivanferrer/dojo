<?php
/**
 * Created by PhpStorm.
 * User: dojo
 * Date: 1/22/16
 * Time: 11:30 AM
 */

namespace Estruturais\Decorator;


// Pão <- recheio <- saladas <- tempero -> pagar

class SubwayLanche
{

}

interface Cost
{
    public function setCost($cost);
    public function getCost();
}

class Pao implements Cost
{
    private $recheios = [];
    private $cost = 0.40;

    public function __construct(Cost $cost = null)
    {
        if(isset($cost)){
        $this->addRecheio($cost);
        }
    }

    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    public function getCost()
    {

        if(count($this->recheios)){

          foreach($this->recheios as $recheio){
            $this->cost += $recheio->getCost();
          }

        }else{
            return  $this->cost;
        }
    }

    public function addRecheio($recheio)
    {
        $this->recheios[] = $recheio;
    }

}


class Salame implements Cost
{
    private $recheios = [];
    private $cost = 2.40;

    public function __construct(Cost $cost = null)
    {
        if(isset($cost)){
            $this->addRecheio($cost);
        }
    }

    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    public function getCost()
    {

        if(count($this->recheios)){

            foreach($this->recheios as $recheio){
                $this->cost += $recheio->getCost();
            }

        }else{
            return  $this->cost;
        }
    }

    public function addRecheio($recheio)
    {
        $this->recheios[] = $recheio;
    }
}


class Tomate implements Cost
{
    private $recheios = [];
    private $cost = 5.50;

    public function __construct(Cost $cost = null)
    {
        if(isset($cost)){
            $this->addRecheio($cost);
        }
    }

    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    public function getCost()
    {

        if(count($this->recheios)){

            foreach($this->recheios as $recheio){
                $this->cost += $recheio->getCost();
            }

        }else{
            return  $this->cost;
        }
    }

    public function addRecheio($recheio)
    {
        $this->recheios[] = $recheio;
    }
}

class Pimenta implements Cost
{

    private $recheios = [];
    private $cost = 0.70;

    public function __construct(Cost $cost = null)
    {
        if(isset($cost)){
            $this->addRecheio($cost);
        }
    }

    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    public function getCost()
    {

        if(count($this->recheios)){

            foreach($this->recheios as $recheio){
                $this->cost += $recheio->getCost();
            }

        }else{
            return  $this->cost;
        }
    }

    public function addRecheio($recheio)
    {
        $this->recheios[] = $recheio;
    }
}


class LancheDecorator implements Cost
{
    private $pao;
    private $recheio;
    private $salada;
    private $tempero;


    /**
     * @param mixed $pao
     */
    public function setPao(Cost $pao)
    {
        $this->pao = $pao;
    }

    /**
     * @return mixed
     */
    public function getPao()
    {
        return $this->pao;
    }

    /**
     * @param mixed $recheio
     */
    public function setRecheio(Cost $recheio)
    {
        $this->recheio = $recheio;
    }

    /**
     * @return mixed
     */
    public function getRecheio()
    {
        return $this->recheio;
    }

    /**
     * @param mixed $salada
     */
    public function setSalada(Cost $salada)
    {
        $this->salada = $salada;
    }

    /**
     * @return mixed
     */
    public function getSalada()
    {
        return $this->salada;
    }

    /**
     * @param mixed $tempero
     */
    public function setTempero(Cost $tempero)
    {
        $this->tempero = $tempero;
    }

    /**
     * @return mixed
     */
    public function getTempero()
    {
        return $this->tempero;
    }

    public function setCost($cost)
    {

    }

    public function getCost()
    {
        static $cost = 0;

        if($this->pao instanceof Cost){
            $cost += $this->pao->getCost();
        }

        if($this->salada instanceof Cost){
            $cost += $this->salada->getCost();
        }

        if($this->tempero instanceof Cost){
            $cost += $this->tempero->getCost();
        }

        if($this->recheio instanceof Cost){
            $cost += $this->recheio->getCost();
        }

        return $cost;

    }


}

$lanche = new LancheDecorator();
$lanche->setPao(new Pao());
$lanche->setRecheio(new Salame());
$lanche->setSalada(new Tomate());
$lanche->setTempero(new Pimenta());

echo $lanche->getCost();

echo "\n";
if(true){
    $lanche->setSalada(new Pimenta);
}
echo "\n";
echo $lanche->getCost();