<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Iterator]
 *
 * @author john-vostro
 */
$myCollelction = [
    new stdClass(),
    new stdClass(),
    new stdClass(),
    new stdClass(),
    new stdClass(),
    new stdClass(),
    new stdClass(),
    new stdClass(),
];

interface MyIteratorInterface
{

    public function hasNext();

    public function next();
}

class MyIterator implements MyIteratorInterface
{
    private $collection = [];
    private $key        = 0;

    public function __construct(array $collection = [])
    {
        $this->collection = $collection;
    }

    public function hasNext()
    {
        return isset($this->collection[$this->key + 1]);
    }

    public function next()
    {
        if ($this->hasNext()) {
            return $this->collection[$this->key++];
        }

        return false;
    }
}
$iterator = new MyIterator($myCollelction);


while ($iterator->hasNext()) {
    var_dump($iterator->next());
}

// Estruturais, Comportamentais e Criacionais
// PHP Iterator



class PHPIterator implements Iterator
{
    private $collection = [];
    private $key        = 0;

    public function __construct(array $collection = [])
    {
        $this->collection = $collection;
    }

    public function current()
    {
        return $this->collection[$this->key];
    }

    public function key()
    {
        return $this->key;
    }

    public function next()
    {
        ++$this->key;
    }

    public function rewind()
    {
        $this->key = 0;
    }

    public function valid()
    {
        return isset($this->collection[$this->key]);
    }
}

$phpIterator = new PHPIterator($myCollelction);

echo "-----------------while----------------------\n";
$phpIterator->rewind();

while ($phpIterator->valid()) {
    var_dump($phpIterator->current());
    $phpIterator->next();
}

echo "-------------------for--------------------\n";


for ($phpIterator->rewind(); $phpIterator->valid(); $phpIterator->next()) {
    var_dump($phpIterator->current());
}


echo "---------------------foreach-----------------\n";

foreach ($phpIterator as $key => $object) {
    var_dump($object);
}