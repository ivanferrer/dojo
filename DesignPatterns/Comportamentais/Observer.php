<?php

class MySingleton implements ISubject
{
    private static $instance = null;
    private $myObservers     = [];

    private function __construct($param1, $param2, $param3)
    {

    }

    private function __clone()
    {

    }

    private function __wakeup()
    {

    }

    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self(null, null, null);
        }

        return self::$instance;
    }

    public function attach(IObserver $observer)
    {
        $mustAddObserver = true;
        if (count($this->myObservers) > 0) {
            foreach ($this->myObservers as $currentObserver) {
                if (spl_object_hash($observer) === spl_object_hash($currentObserver)) {
                    $mustAddObserver = false;
                }
            }
        }

        if ($mustAddObserver) {
            $this->myObservers[] = $observer;
        }

        return $this;
    }

    public function detach(IObserver $observer)
    {
        if (count($this->myObservers) > 0) {
            foreach ($this->myObservers as $key => $currentObserver) {
                if (spl_object_hash($observer) === spl_object_hash($currentObserver)) {
                    unset($this->myObservers[$key]);
                    return;
                }
            }
        }
    }

    public function notify()
    {
        if (count($this->myObservers) > 0) {
            foreach ($this->myObservers as $currentObserver) {
                $currentObserver->update();
            }
        }
    }
}

class MyObserver1 implements IObserver
{

    use Update;
}

class MyObserver2 implements IObserver
{

    use Update;
}

trait Update
{

    function update()
    {
        echo sprintf("%s %s %s %s", time(), microtime(), __CLASS__, "\n");
    }
}

interface IObserver
{

    public function update();
}

interface ISubject
{

    public function attach(IObserver $observer);

    public function detach(IObserver $observer);

    public function notify();
}
$observer1 = new MyObserver1();
$observer2 = new MyObserver2();

$subject = MySingleton::getInstance();

$subject->attach($observer1);
$subject->attach($observer2);

for ($i = 0; $i < 20; $i++) {

    $subject->notify();

    if ($i == 5) {
        $subject->detach($observer2);
        echo "\n----------------------------------------------";
        echo "\n----------- removeu observer 2----------------\n";
        echo "----------------------------------------------\n";
    }

    if ($i == 10) {
        $subject->attach($observer2);
        echo "\n----------------------------------------------";
        echo "\n----------- adicionando observer 2------------\n";
        echo "----------------------------------------------\n";
    }

    if ($i == 15) {
        $subject->detach($observer1);
        echo "\n----------------------------------------------";
        echo "\n----------- removeu observer 1----------------\n";
        echo "----------------------------------------------\n";
    }



    echo "\n----------- ".'$i'." = $i--------------\n";
}
