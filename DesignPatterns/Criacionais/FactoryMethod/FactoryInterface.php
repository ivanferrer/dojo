<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 13/01/16
 * Time: 11:40
 */

namespace Criacionais\FactoryMethod;

interface FactoryInterface
{
    public function fabricate();
}