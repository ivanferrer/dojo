<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 13/01/16
 * Time: 11:35
 */

namespace Criacionais\FactoryMethod;

class Environment
{
    public function getStatus()
    {
        $computer = new ComputerFactory();
        $pc = $computer->fabricate();

        return $pc->boot();
    }

    public function showStatus()
    {
        return $this->getStatus();
    }
}

