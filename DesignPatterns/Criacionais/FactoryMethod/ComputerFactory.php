<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 13/01/16
 * Time: 11:37
 */

namespace Criacionais\FactoryMethod;

class ComputerFactory
{
    public function fabricate()
    {
        $computer = new Computer();
        $computer->setCpu('I7 2.5');
        $computer->setMemory('48Gb');

        return $computer;
    }
}