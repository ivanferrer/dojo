<?php
/**
 * Created by PhpStorm.
 * User: dojo
 * Date: 1/20/16
 * Time: 11:31 AM
 */

namespace Criacionais\Singleton;


class Singleton
{
    private static $instance = null;

    private function __construct()
    {

    }

    private function __clone()
    {

    }

    private function __wakeup()
    {

    }

    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }

        return self::$instance;
    }
}