<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 13/01/16
 * Time: 11:32
 */

namespace Criacionais\AbstractFactory;

class Computer implements ComputerInterface
{
    private $cpu;
    private $memory;

    public function setCpu($cpu)
    {
        $this->cpu = $cpu;
    }

    public function setMemory($memory)
    {
        $this->memory = $memory;
    }

    public function boot()
    {
        return 'booted with cpu: ' . $this->cpu . ' and memory: ' . $this->memory;
    }
}