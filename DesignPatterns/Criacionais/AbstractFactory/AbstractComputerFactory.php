<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 13/01/16
 * Time: 11:37
 */

namespace Criacionais\AbstractFactory;

class AbstractComputerFactory
{
    public static function obterFactory($computer)
    {
//        if ($computer == 'Pc') {
//            return new PcFactory();
//        } else {
//            return new ServerFactory();
//        }
        $computer = ucfirst($computer)."Factory";

        if(!class_exists($computer))
            throw new \RuntimeException("class {$computer} not exists");

        return new $computer;
    }
}