<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 13/01/16
 * Time: 11:35
 */

namespace Criacionais\AbstractFactory;

class Ambiente
{
    public function getStatus($computer)
    {
        $computer = AbstractComputerFactory::obterFactory($computer);
        $pc = $computer->fabricate();

        return $pc->boot();
    }

    public function showStatus()
    {
        return $this->getStatus('Pc');
    }
}

