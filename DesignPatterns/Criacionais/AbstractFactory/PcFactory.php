<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 13/01/16
 * Time: 11:42
 */

namespace Criacionais\AbstractFactory;


class PcFactory
    extends AbstractComputerFactory
    implements FactoryInterface
{
    public function fabricate()
    {
        $computer = new Computer();
        $computer->setCpu('I7 2.5');
        $computer->setMemory('48Gb');

        return $computer;
    }
}