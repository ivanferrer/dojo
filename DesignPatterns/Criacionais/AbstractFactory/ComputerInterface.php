<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 13/01/16
 * Time: 11:38
 */

namespace Criacionais\AbstractFactory;

interface ComputerInterface
{
    public function setCpu($string);
    public function setMemory($string);
    public function boot();
}