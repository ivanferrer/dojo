<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 13/01/16
 * Time: 11:40
 */

namespace Criacionais\AbstractFactory;

interface FactoryInterface
{
    public function fabricate();
}