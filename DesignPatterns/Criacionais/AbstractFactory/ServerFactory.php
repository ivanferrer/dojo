<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 13/01/16
 * Time: 11:42
 */

namespace Criacionais\AbstractFactory;


class ServerFactory
    extends AbstractComputerFactory
    implements FactoryInterface
{
    public function fabricate()
    {
        $computer = new Computer();
        $computer->setCpu('Zion A5');
        $computer->setMemory('256Gb');

        return $computer;
    }
}