<?php
/**
 * Created by PhpStorm.
 * User: dojo
 * Date: 1/15/16
 * Time: 11:52 AM
 */

namespace Criacionais\Builder;


interface ComputerBuilderInterface {
    public function buildComputer();
} 