<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 13/01/16
 * Time: 11:38
 */

namespace Criacionais\Builder;

interface ComputerInterface
{
    public function setCpu($string);
    public function setMemory($string);
    public function enableNetwork($bool);
    public function boot($string);
}