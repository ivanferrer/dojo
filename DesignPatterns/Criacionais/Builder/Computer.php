<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 13/01/16
 * Time: 11:32
 */

namespace Criacionais\Builder;

class Computer implements ComputerInterface
{
    private $cpu;
    private $memory;
    private $network = false;

    public function setCpu($cpu)
    {
        $this->cpu = $cpu;
    }

    public function setMemory($memory)
    {
        $this->memory = $memory;
    }

    public function enableNetwork($bool = false)
    {
        $this->network = $bool;
    }

    public function boot($version = 'latest')
    {
        return 'booted with cpu: ' . $this->cpu . ' and memory: ' . $this->memory . ' version: ' . $this->version;
    }
}