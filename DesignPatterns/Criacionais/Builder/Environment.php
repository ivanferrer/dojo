<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 13/01/16
 * Time: 11:35
 */

namespace Criacionais\Builder;

class Environment
{
    public function bootVm()
    {
        $computer = new ComputerBuilder();
        $computer->enableNetwork(true)
            ->setCpu('I5')
            ->setMemory('12Gb');
        $vm = $computer->buildComputer('14.04');

        return $vm->boot();
    }
}

