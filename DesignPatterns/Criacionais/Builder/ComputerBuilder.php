<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 13/01/16
 * Time: 11:32
 */

namespace Criacionais\Builder;

class ComputerBuilder implements ComputerBuilderInterface
{
    private $cpu;
    private $memory;
    private $network = false;

    public function setCpu($cpu)
    {
        $this->cpu = $cpu;

        return $this;
    }

    public function setMemory($memory)
    {
        $this->memory = $memory;

        return $this;
    }

    public function enableNetwork($bool = false)
    {
        $this->network = $bool;

        return $this;
    }

    public function buildComputer($version = 'latest')
    {
        $computer = new Computer($version);
        $computer->setCpu($this->cpu);
        $computer->setMemory($this->memory);
        $computer->enableNetwork($this->$network);

        return new Computer($version);
    }
}