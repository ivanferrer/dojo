<?php
/**
 * Created by PhpStorm.
 * User: dojo
 * Date: 1/20/16
 * Time: 11:15 AM
 */

namespace Criacionais\Prototype;


class People {

    public function __construct(){
       $this->contacts[] = "contact1";
       $this->contacts[] = "contact2";
       $this->contacts[] = "contact3";
    }

    public $name;
    public $address;
    public $email;
    private $contacts = [];

    public function __clone(){
    $this->contacts = [];
    $this->site = "teste";
    }
}



$peopleOne = new People();

$peopleOne->name = "João da Silva";
$peopleOne->address = "Rua xyz";
$peopleOne->email = "joao@dasilva.com.br";


$peopleTwo = clone $peopleOne;
$peopleTwo->name = "João da Silva Filho";

var_dump($peopleOne, $peopleTwo);
