<?php

//
//class Polimorfismo
//{
//
//}


interface Calcular
{

    public function calcular($valor1, $valor2);
}

class Soma implements Calcular
{

    public function calcular($valor1, $valor2)
    {
        return $valor1 + $valor2;
    }
}

class Subtracao implements Calcular
{

    public function calcular($valor1, $valor2)
    {
        return $valor1 - $valor2;
    }
}

class Multiplicacao implements Calcular
{

    public function calcular($valor1, $valor2)
    {
        return $valor1 * $valor2;
    }
}

class Divisao implements Calcular
{

    public function calcular($valor1, $valor2)
    {
        return $valor1 / $valor2;
    }
}
$calculos[] = new Soma();
$calculos[] = new Subtracao();
$calculos[] = new Multiplicacao();
$calculos[] = new Divisao();


$valor1 = 10;
$valor2 = 5;



foreach ($calculos as $calculo) {

    echo $calculo->calcular($valor1, $valor2)."\n";
}












