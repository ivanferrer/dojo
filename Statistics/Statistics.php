<?php

class Statistics
{

	private $numbers = array();

	public function __construct(array $numbers)
	{
      $this->numbers = $numbers;
	}

	public function getMin()
	{
		return min($this->numbers);	
	}

	public function getMax()
	{
		return max($this->numbers);	
	}

	public function getNumOfElements()
	{
		return count($this->numbers);
	}

	public function getAverage()
	{
		return round(array_sum($this->numbers) / count($this->numbers), 2);
	}

}