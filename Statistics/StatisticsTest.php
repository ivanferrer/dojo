<?php

require "Statistics.php";

class StatisticsTest extends PHPUnit_Framework_TestCase
{
	public function numberProvider()
	{
		return [
			[[1, 2], 1, 2, 2, 1.5, [2]],
			[[2, 3], 2, 3, 2, 2.5],
			[[1, 2, 3], 1, 3, 3, 2],
			[[6, 9, 15, -2, 92, 11], -2, 92, 6, 21.83],
			[[-1, 9, 15, 2, 4, 4], -1, 15, 6, 5.5],

		];
	}

	/**
	 * @dataProvider numberProvider
	 */
	public function testShouldReturnTheMinValue(array $numbers, $expected)
	{
		$statistics = new Statistics($numbers);
		$actual = $statistics->getMin();

		$this->assertEquals($expected, $actual);
	}

	/**
	 * @dataProvider numberProvider
	 */
	public function testShouldReturnTheMaxValue(array $numbers, $skip, $expected)
	{
		$statistics = new Statistics($numbers);
		$actual = $statistics->getMax();

		$this->assertEquals($expected, $actual);	
	}

	/**
	 * 	@dataProvider numberProvider
	 */
	public function testShouldReturnNumOfElements(array $numbers, $skip, $skip, $expected)
	{
		$statistics = new Statistics($numbers);
		$actual = $statistics->getNumOfElements();

		$this->assertEquals($expected, $actual);
	}

	/**
	 * 	@dataProvider numberProvider
	 */
	public function testShouldReturnTheAverage(array $numbers, $skip, $skip, $skip, $expected)
	{
		$statistics = new Statistics($numbers);
		$actual = $statistics->getAverage();

		$this->assertEquals($expected, $actual);
	}

	/**
	 * 	@dataProvider numberProvider
	 */
	public function testShouldReturnThePrimeNumbers(array $numbers, $skip, $skip, $skip, $skip, $expected)
	{
		$statistics = new Statistics($numbers);
		$actual = $statistics->getPrimeNumbers();

		$this->assertEquals($expected, $actual);
	}
}