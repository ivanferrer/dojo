describe("FizzBuzz", function() {
  it("should return 3th as Fizz", function() {
  	var fizzBuzz = new FizzBuzz();

  	var expected = [1,2,'Fizz'];
  	var actual = fizzBuzz.getList(3);

    expect(actual).toEqual(expected);
  });

  it("should return 3th as Fizz and 5th as Buzz", function() {
  	var fizzBuzz = new FizzBuzz();

  	var expected = [1,2,'Fizz',4,'Buzz'];
  	var actual = fizzBuzz.getList(5);

    expect(actual).toEqual(expected);
  });


  it("should return 3th and 6th as Fizz and 5th as Buzz", function() {
  	var fizzBuzz = new FizzBuzz();

  	var expected = [1, 2,'Fizz',4,'Buzz', 'Fizz'];
  	var actual = fizzBuzz.getList(6);

    expect(actual).toEqual(expected);
  });

  it("should return index 14 as FizzBuzz", function() {
  	var fizzBuzz = new FizzBuzz();

  	var expected = 'FizzBuzz';
  	var actual = fizzBuzz.getList(15);

    expect(actual[14]).toEqual(expected);
  });

});