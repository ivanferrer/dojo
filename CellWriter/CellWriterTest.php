<?php
require_once('CellWriter.php');
class CellWriterTest extends PHPUnit_Framework_TestCase
{

    public function letterAndNumberProvider()
    {
        return array(
            array('a', 2),
            array('d', 3),
            array('b', 22),
            array('c', 222),
        );
    }

    /**
     * @dataProvider letterAndNumberProvider
     */
    public function testShouldReturnNumberRelatedToLetter($letter, $number)
    {
        $expected = $number;

        $cellWriter = new CellWriter($letter);
        $actual = $cellWriter->encodeMessage();

        $this->assertEquals($expected, $actual);
    }

    public function testShouldReturnOlaRepresentation()
    {
        $expected = 6665552;

        $cellWriter = new CellWriter('ola');
        $actual = $cellWriter->encodeMessage();

        $this->assertEquals($expected, $actual);
    }

    public function testShouldReturn0Representation()
    {
        $expected = 666555206665552;

        $cellWriter = new CellWriter('ola ola');
        $actual = $cellWriter->encodeMessage();

        $this->assertEquals($expected, $actual);
    }

     public function testShouldReturnUnderlineRepresentation()
    {
        $expected = "2_22_2";

        $cellWriter = new CellWriter('aba');
        $actual = $cellWriter->encodeMessage();

        $this->assertEquals($expected, $actual);
    }
}