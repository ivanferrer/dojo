<?php
class CellWriter
{
    private $text;
    private $keyboard;

    public function __construct($text)
    {
       $this->text = str_split($text);

       $this->keyboard = array(
       		'abc'  => 2,
       		'def'  => 3,
       		'ghi'  => 4,
       		'jkl'  => 5,
       		'mno'  => 6,
       		'pqrs' => 7,
       		'tuv'  => 8,
       		'wxyz' => 9,
       		' ' => 0,
       );
    }

    public function encodeMessage()
    {
    	$ret = '';

    	foreach($this->text as $letter) {
    		foreach($this->keyboard as $key => $value) {
    			$pos = stripos($key, $letter);
    			
    			if ($pos === false) {
    				continue;
    			}

    			$ret .= str_pad($value, $pos+1, $value, STR_PAD_RIGHT);                
    		}
    	}

    	return $ret;
    }
}