<?php
require_once("VampireNumbers.php");

class VampireNumbersTest extends PHPUnit_Framework_TestCase
{

	public function fangsProvider()
	{
		return array(
			array(6, 21, true),
			array(4, 4, false)
		);
	}

	/**
	* @dataProvider fangsProvider
	*/
	public function testShouldValidateFangs($fang1, $fang2, $isVampire)
	{
		$vampireNumbers = new VampireNumbers();

		$this->assertEquals($vampireNumbers->isVampire($fang1,$fang2), $isVampire);
	}	
}