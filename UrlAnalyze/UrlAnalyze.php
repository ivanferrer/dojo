<?php

class UrlAnalyze
{

 private $url;

 public function __construct($url = null)
 {
    $this->url = $url;
 }

 public function run($url)
 {
    return $url;
 }

 public function getProtocol()
 {
   $protocol = explode(":", $this->url)[0];
   return $protocol;
 }
}
