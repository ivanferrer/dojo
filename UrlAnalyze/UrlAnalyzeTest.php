<?php

require 'UrlAnalyze.php';

class UrlAnalyzeTest extends PHPUnit_Framework_TestCase
{
  public function testShouldInstantiate()
  {
    $this->assertInstanceOf('UrlAnalyze', new UrlAnalyze());
  }

  public function testShouldAcceptAString()
  {

    $obj = new UrlAnalyze();

    $url = 'http://www.google.com/mail/user=fulano';
    $expected = $url;

    $value = $obj->run($url);

    $this->assertEquals($expected, $value);
  }

  public function testShouldAcceptValidProtocol()
  {
    $expected = ["http","ssh","https"];

    $url = 'https://www.google.com/mail/user=fulano';
    $obj = new UrlAnalyze($url);

    $actual = $obj->getProtocol();

    $this->assertContains($actual, $expected);
  }

  public function testShouldReturnValidProtocolFromUrl()
  {
    $expected = 'https';

    $url = 'https://www.google.com/mail/user=fulano';
    $obj = new UrlAnalyze($url);

    $actual = $obj->getProtocol();

    $this->assertEquals($actual, $expected);
  }
}
