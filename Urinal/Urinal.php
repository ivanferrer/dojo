<?php

class Urinal
{

  private $urinals;

  public function __construct(array $urinals = null)
  {
    if (is_null($urinals)) {
        throw new InvalidArgumentException();
    }

    foreach($urinals as $urinal){
      if($urinal !== true && $urinal !== false){
        throw new InvalidArgumentException();
      }
    }
    $this->urinals = $urinals;
  }

  public function getAvailablePositions()
  {
    $availablePositions = [];

    foreach($this->urinals as $position => $occupied){
      if($occupied == false
        && ! in_array(($position-1), $availablePositions)
        && $this->urinals[$position] + ((isset($this->urinals[$position]) ? 1 : null)  == false
        ) {

        $availablePositions[] = $position;
      }
    }

    return $availablePositions;
  }
}
