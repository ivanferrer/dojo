<?php

require 'Urinal.php';

class UrinalTest extends PHPUnit_Framework_TestCase
{
  /**
   * @expectedException InvalidArgumentException
   */
  public function testShouldNotInstantiateWithoutAParameter()
  {
    $urinal = new Urinal();
  }

  /**
   * @expectedException InvalidArgumentException
   */

  public function testShouldPositionsShouldAlwaysBeBoolean()
  {
    $positions = [
      'João',
      false,
      true,
      false
    ];

    $urinal = new Urinal($positions);
  }

  public function testShouldReturnOneAsAvailablePositions()
  {
    $positions = [
      false
    ];
    $urinal = new Urinal($positions);

    $expected = [0];
    $actual = $urinal->getAvailablePositions();
    $this->assertEquals($expected, $actual);
  }

  public function testShouldReturnZeroAvailablePositions()
  {
    $positions = [
      true
    ];
    $urinal = new Urinal($positions);

    $expected = [];
    $actual = $urinal->getAvailablePositions();
    $this->assertEquals($expected, $actual);
  }

  public function testShouldReturnTwoAvailablePositionsWhenThreeAreEmpty()
  {
    $positions = [
      false,
      false,
      false
    ];
    $urinal = new Urinal($positions);

    $expected = [0,2];
    $actual = $urinal->getAvailablePositions();
    $this->assertEquals($expected, $actual);
  }


  public function testShouldReturnZeroAvailablePositionsWhenMiddleThreeAreEmpty()
  {
    $positions = [
      false,
      true,
      false
    ];
    $urinal = new Urinal($positions);

    $expected = [];
    $actual = $urinal->getAvailablePositions();
    $this->assertEquals($expected, $actual);
  }

}
