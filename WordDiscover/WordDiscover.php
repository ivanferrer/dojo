<?php

class WordDiscover
{

	private $map;

	public function __construct($map)
	{
		if (! is_array($map) ) {
			throw new InvalidArgumentException();
		}

		$this->map = $map;
	}

	public function getSecret()
	{
		$secret = '';
		foreach ($this->map as $rowKey => $row) {
			foreach ($row as $letter) {

				if (strpos($secret, $letter) !== false) {
					
				}
				$secret .= $letter;
			}
		}
		return $secret;
	}
}