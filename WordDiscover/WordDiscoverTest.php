<?php

require "WordDiscover.php";

class WordDiscoverTest extends PHPUnit_Framework_TestCase
{
	/**
	* @expectedException InvalidArgumentException
	*/
	public function testShouldThrowAnExceptionIfNotAnArray()
	{
		$wordDiscover = new WordDiscover("");
	}

	public function testShouldReturnTup()
	{
		$map = [
			['t','u','p'],
		  ];
		$wordDiscover = new WordDiscover($map);

		$expected = 'tup';
		$actual = $wordDiscover->getSecret();

		$this->assertEquals($expected, $actual);
	}

	public function testShouldReturnThsup()
	{
		$map = [
			['t','u','p'],
			['w','h','s'],
			['h','s','u'],
		  ];
		$wordDiscover = new WordDiscover($map);
		
		$expected = 'thsup';
		$actual = $wordDiscover->getSecret();

		$this->assertEquals($expected, $actual);
	}
}