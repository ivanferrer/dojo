<?php 

require "Atm.php";

class AtmTest extends PHPUnit_Framework_TestCase
{
	public function testShouldReturnAWithdrawOfTenReais()
	{
		$atm = new Atm();
		$expected = [10];
		$actual = $atm->withdraw(10);

		$this->assertEquals($expected,$actual);
	}

	public function testShouldReturnAWithdrawOf30Reais()
	{
		$expected = [20, 10];

		$atm = new Atm();
		$actual = $atm->withdraw(30);

		$this->assertEquals($expected, $actual);
	}

	public function testShouldReturnAWithdrawOf40Reais()
	{
		$expected = [20, 20];

		$atm = new Atm();
		$actual = $atm->withdraw(40);

		$this->assertEquals($expected, $actual);
	}
}