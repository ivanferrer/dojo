<?php

class Atm
{
	private $value = [10,20,50,100];

	public function withdraw($withdraw)
	{
		$return = [];

		while($withdraw > 0) {
			for($i = count($this->value) -1; $i >= 0 ; $i--){				
				if($this->value[$i] <= $withdraw){
					$return[] = $this->value[$i];
					$withdraw -= $this->value[$i];
				}
			}
		}

		return $return;
	}
}