<?php

require 'BattleShip.php';

class BattleShipTest extends PHPUnit_Framework_TestCase
{
    public function testShouldInstantiateBattleShip()
    {
        $battleShip = new BattleShip();

        $this->assertInstanceOf('BattleShip', $battleShip);

        return $battleShip;
    }

    public function testShouldReturnPlayersShipsPositions()
    {
        $p1 = [
            ['a1','c1']
        ];
        $p2 = [
            ['a1','c1'],
        ];

        $battleShip = new BattleShip();

        $battleShip->setPlayersShipsPositions($p1, $p2);

        $p1Ships = $battleShip->getPlayerShips(1);
        $p2Ships = $battleShip->getPlayerShips(2);

        $this->assertEquals($p1, $p1Ships);
        $this->assertEquals($p2, $p2Ships);
    }

    /**
    * @expectedException InvalidArgumentException
    */

    public function testShouldReturnExceptionWhenPositionIsUsed()
    {
        $p1 = [
            ['a1','c1'],
            ['b1', 'b2']
        ];

        $p2 = [
            ['a1','c1'],
            ['b1', 'b2']
        ];

        $battleShip = new BattleShip();

        $battleShip->setPlayersShipsPositions($p1, $p2);

    }
}