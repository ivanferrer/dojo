<?php

class AssassinDiscover
{
    private $availableAssassins = [
        1 => 'Charles B. Abbage',
        2 => 'Donald Duck Knuth',
        3 => 'Ada L. Ovelace',
        4 => 'Alan T. Uring',
        5 => 'Ivar J. Acobson',
        6 => 'Ras Mus Ler Dorf',
    ];

    private $availableLocals = [
        1 => 'Redmond',
        2 => 'Palo Alto',
        3 => 'San Francisco',
        4 => 'Tokio',
        5 => 'Restaurante no Fim do Universo',
        6 => 'São Paulo',
        7 => 'Cupertino',
        8 => 'Helsinki',
        9 => 'Maida Vale',
        10 => 'Toronto'
    ];

    private $availableWeapons = [
        1 => 'Peixeira',
        2 => 'DynaTAC 8000X (o primeiro aparelho celular do mundo)',
        3 => 'Trezoitão',
        4 => 'Trebuchet',
        5 => 'Maça',
        6 => 'Gládio'
    ];


    public function __construct($assassin, $weapon, $local)
    {
        $this->assassin = $assassin;
        $this->weapon = $weapon;
        $this->local = $local;

       if ($message = $this->isCrimeSceneValid()) {
           throw new InvalidArgumentException($message);
       }
    }

    public function discoverAssassin($theory)
    {
        if ($this->availableAssassins[$theory[0]] != $this->assassin) {
            return 1;
        }

        if ($this->availableWeapons[$theory[1]] != $this->weapon) {
            return 2;
        }

        if ($this->availableLocals[$theory[2]] != $this->local) {
            return 3;
        }

        return 0;
    }

    private function isCrimeSceneValid()
    {
        if (! in_array($this->assassin, $this->availableAssassins)) {
            return 'Wrong assassin';
        }
        if (! in_array($this->local, $this->availableLocals)) {
            return 'Wrong local';
        }
        if (! in_array($this->weapon, $this->availableWeapons)) {
            return 'Wrong weapon';
        }

        return false;
    }
}