<?php

require "AssassinDiscover.php";

class AssassinDiscoverTest extends PHPUnit_Framework_TestCase
{
	public function assertPreConditions()
	{
		$this->assertTrue(class_exists($class = "AssassinDiscover"),
			"Class $class not found.");
	}

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldInstantiateAssassinDiscoverWithParameterCrimeWithInvalidArgumentException()
    {
        new AssassinDiscover('Not an assassin', 'Trezoitão', 'Tokio');
    }

    public function testShouldInstantiateAssassinDiscoverWithParameterCrimeWithValidation()
    {
        return new AssassinDiscover('Donald Duck Knuth', 'Trezoitão', 'Tokio');
    }

    public function theoryProvider()
    {
        return array(
            array([1, 1, 1], 1), // wrong assassin, wrong weapon, wrong local
            array([2, 1, 1], 2), // right assassin, wrong weapon, wrong local
            array([2, 2, 1], 2), // right assassin, wrong weapon, wrong local
            array([2, 3, 1], 3), // right assassin, right weapon, wrong local
            array([2, 3, 2], 3), // right assassin, right weapon, wrong local
            array([2, 3, 4], 0), // all right
        );
    }

    /**
     * @dataProvider theoryProvider
     * @depends testShouldInstantiateAssassinDiscoverWithParameterCrimeWithValidation
     */
    public function testShouldReturnNumberOfWrongParam($theory, $expected, $instance)
    {
        $actual = $instance->discoverAssassin($theory);

        $this->assertEquals($expected, $actual);
    }

    public function testShouldReturnZeroOnRightParams()
    {
        $instance = new AssassinDiscover('Charles B. Abbage', 'Peixeira','Redmond');

        $actual = $instance->discoverAssassin([1,1,1]);
        $expected = 0;

        $this->assertEquals($expected, $actual);
    }
}

