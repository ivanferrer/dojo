<?php

require 'FindingArea.php';

class FindingAreaTest extends PHPUnit_Framework_TestCase
{
	public function testShouldReturnSquareArea()
	{
		$expected = 4;
		$findingArea = new FindingArea('square');
		$actual = $findingArea->calcArea(2);

		$this->assertEquals($actual, $expected);
	}

	public function testShouldReturnRectangleArea()
	{
		$expected = 6;
		$findingArea = new FindingArea('rectangle');
		$actual = $findingArea->calcArea(2,3);

		$this->assertEquals($actual, $expected);
	}

	public function testShouldReturnCircleArea()
	{
		$expected = 12.56;
		$findingArea = new FindingArea('circle');
		$actual = $findingArea->calcArea(2);

		$this->assertEquals($actual, $expected);
	}

	public function testShouldReturnEllipseArea()
	{
		$expected = 18.84;
		$findingArea = new FindingArea('ellipse');
		$actual = $findingArea->calcArea(2,3);

		$this->assertEquals($actual, $expected);
	}

	public function testShouldReturnTrapezoidArea()
	{
		$expected = 7;
		$findingArea = new FindingArea('trapezoid');
		$actual = $findingArea->calcArea(2,4,3);

		$this->assertEquals($actual, $expected);
	}

	public function testShouldReturnTriangleArea()
	{
        $expected = 3;
		$findingArea = new FindingArea('triangle');
		$actual = $findingArea->calcArea(2,3);

		$this->assertEquals($actual, $expected);
	}

	public function testShouldReturnParalelogramArea()
	{
        $expected = 3;
		$findingArea = new FindingArea('triangle');
		$actual = $findingArea->calcArea(2,3);

		$this->assertEquals($actual, $expected);
	}
}