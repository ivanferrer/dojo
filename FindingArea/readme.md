FindingArea

Dojo consiste em achar a area dependendo do formato geometrico passado e os valores necessários para se calcular a area daquela forma.

A entrada deverá ser e os valores defaults são:

Formato         Valor                                   Calculo
square          a tamanho do lado                       A = a²
rectangle       b tamanho da base e a altura            A = ab
parallelogram   b tamanho da base e h altura            A = bh
circle          r o raio                                A = (pi)r²
ellipse         r1 o raio maior r2 raio menor           A = (pi)r1r2
trapezoid       h altura b1 base maior b2 base menor    A = (h([b1 + b2])/2
triangle        b base e h altura                       A = bh/2

to be continue...
