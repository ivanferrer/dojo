<?php

class FindingArea
{

	private $shape;

	public function __construct($shape)
	{
		$this->shape = $shape;
	}

	public function calcArea()
	{
		$args = func_get_args();

		return $this->{"calc".$this->shape}($args);
	}

	private function calcCircle($args)
	{
        return 3.14 * pow($args[0], 2);
	}

	private function calcSquare($args)
	{
        return pow($args[0], 2);
	}

	private function calcRectangle($args)
	{
        return $args[0] * $args[1];
	}

	private function calcEllipse($args)
	{
        return 3.14 * $args[0] * $args[1];
	}

	private function calcTrapezoid($args)
	{
		return $args[0] * ($args[1] + $args[2]) / 2;
	}

	private function calcTriangle($args)
	{
		return $func_get_args[0] * $args[1] / 2;
	}
}