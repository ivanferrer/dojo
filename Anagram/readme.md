Escreva um programa que gere todos os anagramas potenciais de uma string.
Por exemplo, os anagramas potenciais de "biro" são:
biro bior brio broi boir bori
ibro ibor irbo irob iobr iorb
rbio rboi ribo riob roib robi
obir obri oibr oirb orbi orib

