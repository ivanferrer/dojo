<?php

require "Anagram.php";

class AnagramTest extends PHPUnit_Framework_TestCase
{
	private function arrays_are_similar($a, $b) {
  // if the indexes don't match, return immediately
  if (count(array_diff_assoc($a, $b))) {
    return false;
  }
  // we know that the indexes, but maybe not values, match.
  // compare the values between the two arrays
  foreach($a as $k => $v) {
    if ($v !== $b[$k]) {
      return false;
    }
  }
  // we have identical indexes, and no unequal values
  return true;
}

   public function testShouldReturnOiIoForOi()
   {
       $anagram = new Anagram("oi");
       $expected = array('oi','io');

       $actual = $anagram->result();
       $this->assertTrue($this->arrays_are_similar($expected, $actual)); 
   }

   public function testShouldReturnAiIaForAi()
   {
   		$anagram = new Anagram('ai');
   		$expected = array('ai', 'ia');

   		$actual = $anagram->result();
       $this->assertTrue($this->arrays_are_similar($expected, $actual)); 
   }


   public function testShouldReturnAlaAalLaaForAla()
   {
   		$anagram = new Anagram('ala');
   		$expected = array('ala', 'laa', 'aal');

   		$actual = $anagram->result();

       $this->assertTrue($this->arrays_are_similar($expected, $actual)); 
   }
}