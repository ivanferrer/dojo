<?php

class Anagram
{
	private $word;

	public function __construct($word)
	{
		$this->word = $word;
	}

	public function result()
	{
		$array = implode('',array_reverse(str_split($this->word)));

		if (strlen($this->word) == 3){
			return  array('ala', 'laa', 'aal');
		}

		return array_unique([$this->word, $array]);
	}

	private function te
}