<?php
include('HappyNumbers.php');
class HappyNumbersTest extends PHPUnit_Framework_TestCase
{
  public function testShouldReturnSad()
  {
    $happyNumbers = new HappyNumbers();

    $expect = 'sad';
    $result = $happyNumbers->isHappy(0);

    $this->assertEquals($expect, $result);
  }

  public function testShouldReturnHappyWhenOne()
  {
    $happyNumbers = new HappyNumbers();

    $expect = 'happy';
    $result = $happyNumbers->isHappy(1);

    $this->assertEquals($expect, $result);
  }

  public function testShouldReturnSadWhenTwo()
  {
    $happyNumbers = new HappyNumbers();

    $expect = 'sad';
    $result = $happyNumbers->isHappy(232);

    $this->assertEquals($expect, $result);
  }
}

/*
2 = 4
4 = 16
1² + 6² = 37
3² + 7² = 58
5² + 8² = 89
8² + 9² = 145
1 + 4 + 5 = 42
4 + 2 = 20
2² + 0 = 4
*/
